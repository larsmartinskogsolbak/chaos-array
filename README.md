A console application consisting of a custom collection that can story any type. 

The Insert method tries to insert an element at an random position in the created ChaosArray object. 

The GetItem method tries to get an element at a random position in the ChaosArray object.

Custom exceptions are thrown and caught if the methods tries to insert items at unavailable positions or tries to retrieve a non-existent item. 