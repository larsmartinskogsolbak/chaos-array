﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My_Collections
{
    class ArrayGetException : Exception
    {
        public ArrayGetException()
            : base("Error") { }
        public ArrayGetException(string message)
            : base(message) { }

    }
}
