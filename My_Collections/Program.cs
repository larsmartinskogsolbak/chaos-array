﻿using System;

namespace My_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<int> listInt = new ChaosArray<int>(10);
            try
            {
                for (int i = 1; i < 11; i++)
                {
                    listInt.Insert(i);
                }
            }
            catch (ArrayInsertException)
            {
                Console.WriteLine("Array insert error");
            }

             try
            {
                for (int i = 0; i < 3; i++)
                {
                    Console.WriteLine(listInt.GetItem());
                }
            }
            catch (ArrayGetException) {
                Console.WriteLine("Cannot Get int");
            };
            
 
            //listInt.Print();


            ChaosArray<string> listString = new ChaosArray<string>(5);
            try
            {
                listString.Insert("a");
                listString.Insert("b");
                listString.Insert("c");
                listString.Insert("d");
                listString.Insert("e");
            }
            catch (ArrayInsertException)
            {
                Console.WriteLine("Array insert Error");
            }

            try
            {
                Console.WriteLine(listString.GetItem());
                Console.WriteLine(listString.GetItem());
                Console.WriteLine(listString.GetItem());
                //Console.WriteLine(listString.GetItem());
                //Console.WriteLine(listString.GetItem());
            }
            catch (ArrayGetException) {
                Console.WriteLine("Cannot Get string");
            };

            //listString.Print();

        }
    }
}
