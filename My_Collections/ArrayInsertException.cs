﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My_Collections
{
    class ArrayInsertException : Exception
    {
        public ArrayInsertException()
        {
           
        }
        public ArrayInsertException(string message) 
            : base(message) { } 
    }
}
