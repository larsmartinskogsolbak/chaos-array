﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace My_Collections
{
    class ChaosArray<T>
    {
        private int size;
        private T[] array;
        private bool[] indexFull;

        public ChaosArray(int size)
        {
            this.size = size;
            array = new T[size];
            indexFull = new bool[size];
        }

        public void Insert(T item)
        {
            Random rand = new Random();
            int index = rand.Next(0, size);
            if (indexFull[index])
            {
                throw new ArrayInsertException();
            }
            else
            {
                array[index] = item;
                indexFull[index] = true;
            }
            
        }

        public T GetItem()
        {
            int index = new Random().Next(1, size);

            if (indexFull[index])
            {
                return array[index];
            }
            else
            {
                throw new ArrayGetException();
            }
          
        }
       public void Print()
        {
             for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }
    }
}